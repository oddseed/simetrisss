from django.shortcuts import render
from miembro.models import Miembro
from enlace.models import Enlace

def Directorio(request):
	miembros = Miembro.objects.all()
	enlace = Enlace.objects.all()
	return render(request, 'directorio.html', {'miembros': miembros,'enlaces':enlace})

