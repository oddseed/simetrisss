from django.db import models
from site_root import SITE_GROOT

class Miembro(models.Model):
	nombre = models.CharField(max_length=150)
	cargo = models.CharField(max_length=150)
	telefono = models.CharField(max_length=150)
	email = models.EmailField()
	img = models.ImageField(upload_to=SITE_GROOT,null=True,default="",blank=True)
	
	def __unicode__(self):
		return self.nombre
	
