from django.db import models

class Enlace(models.Model):
	nombre = models.CharField(max_length=75)
	url = models.CharField(max_length=150)
	
	def __unicode__(self):
		return self.nombre