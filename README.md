###Pagina web de SIMETRISSS ###
Usaremos [Python][3] y el framework web [Django][1] para desarrollar el siguiente proyecto.

![SIMETRISS][2]
### BASE DE DATOS ###
El usuario admin de la base del proyecto que trae es:
user: administrador
pass: 12345

### GIT PULL ###
Un error como el siguiente se da por sobre escritura de archivos ... se soluciona borrando los archivos que estan locales para que los del repositorio los sobra escriban.

	error: Your local changes to the following files would be overwritten by merge:
		Desarrollo.db
		SIMETRISSS/settings.pyc
		SIMETRISSS/urls.pyc
		menu/models.pyc
		noticia/models.pyc
		pagina/models.pyc
		pagina/views.pyc
	Please, commit your changes or stash them before you can merge.
	Aborting


###Requisitos###
    * Python 2.7
    * Django 1.5.1
    * Django Suit 0.2.1
    * Sqlite3 (Development DB)
    * MySql (Deployment DB)

### Instalacion de el ambiente de trabajo ###
Usando un sistema operativo Unix

Creamos un ambiente virtual

	$ virtualenv django

Entramos al ambiente virtual

	$ source django/bin/activate

*(django)* nos indica que estamos dentro del ambiente que creamos llamado django.

Instalamos django

	(django)$ pip install django django-suit
	...
	Successfully installed django
	Cleaning up...

Probamos que tenemos la version correcta
	
	(django)$ python -c 'from django import get_version;print get_version()'
	1.5

Lo cual es equivalente a:

	(django)$ python
	Python 2.7.3 (default, Aug  9 2012, 17:23:58) 
	[GCC 4.7.1 20120720 (Red Hat 4.7.1-5)] on linux2
	Type "help", "copyright", "credits" or "license" for more information.
	>>> import django
	>>> print django.get_version()
	1.5

Preguntas [oddseed@gmail.com][4]

[1]: https://www.djangoproject.com/download/
[2]: http://img694.imageshack.us/img694/8178/logotiposimetrisss.png
[3]: http://python.org
[4]: mailto:oddseed@gmail.com
