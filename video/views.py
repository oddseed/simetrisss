from django.shortcuts import render
from enlace.models import Enlace
from video.models import Video
from django.http import HttpResponse

def ListaVideos(request):    
    enlace = Enlace.objects.all()
    vids = Video.objects.all()
    return render(request, 'listavideos.html', {'enlaces': enlace, 'vids':vids})

def Videos(request,pvid):
    v = Video.objects.get(pk=pvid)
    enlace = Enlace.objects.all()
    return render(request, 'video.html', {'enlaces': enlace, 'video':v})
    

