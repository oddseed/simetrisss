from django.db import models

class Video(models.Model):
	titulo = models.CharField(max_length=150)
	descripcion = models.TextField(null=True, blank=True, default = '')
	url = models.CharField(max_length=150)
	
	def __unicode__(self):
		return self.titulo


	
