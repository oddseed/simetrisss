from django.shortcuts import render
from enlace.models import Enlace
from documento.models import Documento


def Documentos(request):
    enlace = Enlace.objects.all()
    docs = Documento.objects.all()
    return render(request, 'documentos.html', {'docs': docs, 'enlaces': enlace})