from django.db import models
from site_root import SITE_FROOT

class Documento(models.Model):
	titulo = models.CharField(max_length=150)
	desc = models.TextField(null=True, blank=True, default = '')
	url = models.FileField(upload_to=SITE_FROOT)
	
	def __unicode__(self):
		return self.titulo
	
