# -*- coding: latin-1 -*-
from django.shortcuts import render
from enlace.models import Enlace
from publicacion.models import Publicacion
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from pagina.models import Pagina
from django.http import HttpResponseRedirect

from django import forms
class SearchPublicacion(forms.Form):
    buscar = forms.CharField(required=False)

def PaginaPrincipal(request,page =1):
    if request.POST:
        return HttpResponseRedirect('/busqueda/'+request.POST['buscar'])
    else:
        publicacion = Publicacion.objects.all().order_by('-fecha')
        paginador = Paginator(publicacion, 5)
        enlace = Enlace.objects.all()
        busqueda = SearchPublicacion()
        try:
            pub = paginador.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            pub = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            pub = paginador.page(paginador.num_pages)
    
        return render(request, 'index.html', {'publicaciones': pub,'enlaces':enlace,'busqueda':busqueda})

def Organigrama(request):    
    enlace = Enlace.objects.all()
    pagina = Pagina.objects.filter(titulo__exact='Organigrama')
    if pagina:
        pagina = pagina[0]
        return render(request, 'pagina.html', {'pagina': pagina,'enlaces':enlace})
    else:
        return render(request, 'pagina.html', {'enlaces':enlace})

def Ubicacion(request):    
    enlace = Enlace.objects.all()
    pagina = Pagina.objects.filter(titulo__exact='Ubicación')
    if pagina:
        pagina = pagina[0]
        return render(request, 'pagina.html', {'pagina': pagina,'enlaces':enlace})
    else:
        return render(request, 'pagina.html', {'enlaces':enlace})

def Contactenos(request):    
    enlace = Enlace.objects.all()
    pagina = Pagina.objects.filter(titulo__exact='Contactenos')
    if pagina:
        pagina = pagina[0]
        return render(request, 'pagina.html', {'pagina': pagina,'enlaces':enlace})
    else:
        return render(request, 'pagina.html', {'enlaces':enlace})

def Estatutos(request):    
    enlace = Enlace.objects.all()
    pagina = Pagina.objects.filter(titulo__exact='Estatutos')
    if pagina:
        pagina = pagina[0]
        return render(request, 'pagina.html', {'pagina': pagina,'enlaces':enlace})
    else:
        return render(request, 'pagina.html', {'enlaces':enlace})

def MisionVision(request):
    enlace = Enlace.objects.all()
    pagina = Pagina.objects.filter(titulo__exact='Misión y Visión')
    if pagina:
        pagina = pagina[0]
        return render(request, 'pagina.html', {'pagina': pagina,'enlaces':enlace})
    else:
        return render(request, 'pagina.html', {'enlaces':enlace})

def Busqueda(request):
    enlace = Enlace.objects.all()
    pagina = Pagina.objects.filter(titulo__exact='Misión y Visión')
    if pagina:
        pagina = pagina[0]
        return render(request, 'pagina.html', {'pagina': pagina,'enlaces':enlace})
    else:
        return render(request, 'pagina.html', {'enlaces':enlace})

from django.views.generic import ListView

class Busqueda(ListView):
    model = Publicacion
    form_class = SearchPublicacion
    context_object_name = 'publicacion'
    template_name = 'busqueda.html'
    pubs = []
    
    def get_queryset(self):
            if len(self.args) > 0:
                return Publicacion.objects.filter(titulo__icontains=self.args[0])
            else:
                return Publicacion.objects.filter()
            