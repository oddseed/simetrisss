from django.db import models
from suit_ckeditor.widgets import CKEditorWidget

class Pagina(models.Model):
    titulo = models.CharField(max_length=150)
    contenido = models.TextField(null=True, blank=True, default = '')

    def __unicode__(self):
        return self.titulo
    