from django.contrib import admin
from pagina.models import Pagina
from django.forms import ModelForm
from django.contrib.admin import ModelAdmin
from suit_ckeditor.widgets import CKEditorWidget

class PageForm(ModelForm):
    class Meta:
        widgets = {
            'contenido': CKEditorWidget(editor_options={'startupFocus': True,})
        }

class PageAdmin(ModelAdmin):
    form = PageForm
    fieldsets = [
      ('Pagina', {'classes': ('full-width',), 'fields': ('titulo','contenido')}),
    ]
    def has_add_permission(self, request):
        return False
    def has_delete_permission(self, request,obj=None):
        return False
    
admin.site.register(Pagina, PageAdmin)
     