from django.db import models

class Evento(models.Model):
	fecha = models.DateField()
	titulo = models.CharField(max_length=150)
	descripcion = models.TextField(null=True, blank=True, default = '')
	
	def __unicode__(self):
		return self.titulo
	
