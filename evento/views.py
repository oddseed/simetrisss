# Create your views here.
#No se que estoy haciendo
from django.shortcuts import render
from evento.models import Evento
from enlace.models import Enlace
# Create your views here.

def ActividadesRealizadas(request):
    from datetime import datetime
    now = datetime.now()
    enlace = Enlace.objects.all()
    eventos = Evento.objects.filter(fecha__lt=now).order_by('-fecha')
    return render(request, 'eventos.html', {'eventos': eventos, 'enlaces':enlace})

def ProximosEventos(request):
    from datetime import datetime
    now = datetime.now()    
    enlace = Enlace.objects.all()
    eventos = Evento.objects.filter(fecha__gte=now).order_by('+fecha')
    return render(request, 'eventos.html', {'eventos': eventos, 'enlaces':enlace})