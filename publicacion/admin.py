from django.contrib import admin
from publicacion.models import Publicacion,TipoPublicacion
from django.forms import ModelForm
from django.contrib.admin import ModelAdmin
from suit_ckeditor.widgets import CKEditorWidget

class PublicacionForm(ModelForm):
    class Meta:
        widgets = {
            'contenido': CKEditorWidget(editor_options={'startupFocus': True})
        }

class PublicacionAdmin(ModelAdmin):
    form = PublicacionForm
    list_display = ('titulo', 'tipo')
    fieldsets = [
      ('Publicacion', {'classes': ('full-width',), 'fields': ('tipo','fecha','titulo','contenido')}),
    ]

admin.site.register(Publicacion, PublicacionAdmin)