from django.shortcuts import render
from enlace.models import Enlace
from publicacion.models import Publicacion
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def EducacionSindical(request,page=1):
    enlace = Enlace.objects.all()
    pubs = Publicacion.objects.filter(tipo=4).order_by('-fecha')
    paginador = Paginator(pubs, 5)
    try:
        pub = paginador.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        pub = paginador.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        pub = paginador.page(paginador.num_pages)

    return render(request, 'publicaciones_4.html', {'publicaciones': pub,'enlaces':enlace})

def EducacionMedica(request,page=1):    
    enlace = Enlace.objects.all()
    pubs = Publicacion.objects.filter(tipo=3).order_by('-fecha')
    paginador = Paginator(pubs, 5)
    try:
        pub = paginador.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        pub = paginador.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        pub = paginador.page(paginador.num_pages)

    return render(request, 'publicaciones_3.html', {'publicaciones': pub,'enlaces':enlace})


def Comunicado(request,page=1):
    enlace = Enlace.objects.all()
    pubs = Publicacion.objects.filter(tipo=2).order_by('-fecha')
    paginador = Paginator(pubs, 5)
    try:
        pub = paginador.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        pub = paginador.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        pub = paginador.page(paginador.num_pages)

    return render(request, 'publicaciones_2.html', {'publicaciones': pub,'enlaces':enlace})

def Editorial(request,page=1):
    enlace = Enlace.objects.all()
    pubs = Publicacion.objects.filter(tipo=1).order_by('-fecha')
    paginador = Paginator(pubs, 5)
    try:
        pub = paginador.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        pub = paginador.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        pub = paginador.page(paginador.num_pages)

    return render(request, 'publicaciones_1.html', {'publicaciones': pub,'enlaces':enlace})

def Post(request,page=1):
    try:
        enlace = Enlace.objects.all()
        pub = Publicacion.objects.get(pk=page)
        return render(request, 'publicaciones.html', {'item': pub,'enlaces':enlace})
    except:
        return HttpResponseRedirect('/')
