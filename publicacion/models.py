from django.db import models

class TipoPublicacion(models.Model):    
    name = models.CharField(max_length=75)
    def __unicode__(self):
        return self.name

class Publicacion (models.Model):
    class Meta:
                verbose_name_plural = "Publicaciones"    
    tipo = models.ForeignKey(TipoPublicacion)
    fecha = models.DateField()
    titulo =  models.CharField(max_length=150)
    contenido = models.TextField(null=True, blank=True, default = '')

    def __unicode__(self):
        return self.titulo
