from django.conf.urls import patterns, include, url
import pagina.views
from django.conf import settings
from pagina.views import Busqueda
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'pagina.views.PaginaPrincipal', name='index'),
    url(r'^(?P<page>\d+)/$', 'pagina.views.PaginaPrincipal', name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    #INSTITUCIONAL
    url(r'^organigrama/$', 'pagina.views.Organigrama', name='organigrama'),
    url(r'^misionvision/$', 'pagina.views.MisionVision', name='misionvision'),
    url(r'^estatutos/$', 'pagina.views.Estatutos', name='estatutos'),
    #PUBLICACIONES
    url(r'^documentos/$', 'documento.views.Documentos', name='documentos'),
    url(r'^editoriales/$', 'publicacion.views.Editorial', name='editorial'),
    url(r'^comunicados/$', 'publicacion.views.Comunicado', name='comunicado'),
    url(r'^educacionmedica/$', 'publicacion.views.EducacionMedica', name='educionmedica'),
    url(r'^educacionsindical/$', 'publicacion.views.EducacionSindical', name='educacionsindical'),
    url(r'^documentos/(?P<page>\d+)/$', 'documento.views.Documentos', name='documentos'),
    url(r'^editoriales/(?P<page>\d+)/$', 'publicacion.views.Editorial', name='editorial'),
    url(r'^comunicados/(?P<page>\d+)/$', 'publicacion.views.Comunicado', name='comunicado'),
    url(r'^educacionmedica/(?P<page>\d+)/$', 'publicacion.views.EducacionMedica', name='educionmedica'),
    url(r'^educacionsindical/(?P<page>\d+)/$', 'publicacion.views.EducacionSindical', name='educacionsindical'),
    url(r'^post/(?P<page>\d+)/$', 'publicacion.views.Post', name='post'),
    
    #EVENTOS
    url(r'^proximoseventos/$', 'evento.views.ProximosEventos', name='proximosventos'),
    url(r'^actividadesrealizadas/$', 'evento.views.ActividadesRealizadas', name='actividadesrealizadas'),
    url(r'^galeria/$', 'imagen.views.Galeria', name='galeria'),
    url(r'^videos/$', 'video.views.ListaVideos', name='listavideos'),
    url(r'^videos/(?P<pvid>\d+)/$', 'video.views.Videos', name='videos'),
    url(r'^busqueda/(\w+)/$', Busqueda.as_view()),
    url(r'^busqueda/$','pagina.views.PaginaPrincipal', name='index'),
    #ENLACES
    #CONTACTENOS
    url(r'^contactenos/$', 'pagina.views.Contactenos', name='contactenos'),
    url(r'^ubicacion/$', 'pagina.views.Ubicacion', name='ubicacion'),
    url(r'^directorio/$', 'miembro.views.Directorio', name='directorio'),
    )
