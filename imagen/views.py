from django.shortcuts import render
from imagen.models import Imagen
from enlace.models import Enlace
# Create your views here.

def Galeria(request):    
    enlace = Enlace.objects.all()
    imgs = Imagen.objects.all()
    return render(request, 'galeria.html', {'enlaces': enlace,'imgs':imgs})