from django.db import models
from evento.models import Evento
from site_root import SITE_GROOT

class Imagen(models.Model):
	fecha = models.DateField()
	descripcion = models.TextField(null=True, blank=True, default = '')
	url = models.ImageField(upload_to=SITE_GROOT)
	evento = models.ForeignKey(Evento,null=True, blank=True, default = None)
	class Meta:
			verbose_name_plural = "Imagenes"	
	
	def __unicode__(self):
		return str(self.url)
	
